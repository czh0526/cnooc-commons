package ref

import (
	"encoding/json"
	"fmt"

	"gitee.com/czh0526/cnooc-commons/utils"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

const (
	TYPE_INTERNAL = "internal"
	TYPE_EXTERNAL = "external"
)

type Metadata struct {
	Type string `json:"type"`
	Desc string `json:"desc,omitempty"`
}

type Reference struct {
	ID   string `json:"id"`
	Type string `json:"type"`
	Desc string `json:"desc,omitempty"`
}

func (ref *Reference) CopyFrom(metadata *Metadata) {
	ref.Type = metadata.Type
	ref.Desc = metadata.Desc
}

func UpdateRef(ctx contractapi.TransactionContextInterface, dataKey string, refDataKey string, metadata Metadata, checkExists bool) error {

	if checkExists {
		// 确认数据对象存在
		data, _ := ctx.GetStub().GetState(dataKey)
		if data == nil {
			return fmt.Errorf("data '%v' does not exist", dataKey)
		}
	}

	if metadata.Type == TYPE_INTERNAL {
		// 确认引用对象存在
		refData, _ := ctx.GetStub().GetState(refDataKey)
		if refData == nil {
			return fmt.Errorf("reference '%v' does not exist", refDataKey)
		}
	}

	refData, err := json.Marshal(&metadata)
	if err != nil {
		return fmt.Errorf("marshal 'ref.Metadata' error: %s", err)
	}

	// data => refData
	dataRefKey := utils.MakeRefKey(dataKey, refDataKey)
	if err := ctx.GetStub().PutState(dataRefKey, refData); err != nil {
		return err
	}
	fmt.Printf("[state] %s => %s \n", dataRefKey, refData)

	if metadata.Type == TYPE_INTERNAL {
		// refData => data
		refDataRefKey := utils.MakeRefKey(refDataKey, dataKey)
		if err := ctx.GetStub().PutState(refDataRefKey, refData); err != nil {
			return err
		}
		fmt.Printf("[state] %s => %s \n", refDataRefKey, refData)
	}

	return nil
}

func LoadRefs(ctx contractapi.TransactionContextInterface, dataKey string) (*map[string]Metadata, error) {

	var err error

	startKey := utils.MakeRefKey(dataKey, " ")
	endKey := utils.MakeRefKey(dataKey, "~")
	refIter, err := ctx.GetStub().GetStateByRange(startKey, endKey)
	if err != nil {
		return nil, err
	}
	defer refIter.Close()

	refs := map[string]Metadata{}
	for refIter.HasNext() {

		kv, err := refIter.Next()
		if err != nil {
			return nil, err
		}

		metadata := Metadata{}
		if err = json.Unmarshal(kv.GetValue(), &metadata); err != nil {
			return nil, err
		}
		refs[kv.GetKey()] = metadata
	}

	return &refs, nil
}
