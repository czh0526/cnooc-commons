package utils

import "strings"

func SplitKey(key string) []string {
	return strings.Split(key, ":")
}

func MakeKey(keyParts ...string) string {
	return strings.Join(keyParts, ":")
}

func MakeRefKey(dataKey string, refDataKey string) string {
	return MakeKey(dataKey, "refs["+refDataKey+"]")
}
